module.exports = {
  up: queryInterface => {
    return queryInterface.sequelize.query(`
      INSERT INTO "Users" (login, password, age) VALUES ('admin', 'admin1', 23);
      INSERT INTO "Users" (login, password, age) VALUES ('mike', 'mike1', 54);
      INSERT INTO "Users" (login, password, age) VALUES ('freddy', 'freddy1', 43);
      INSERT INTO "Users" (login, password, age) VALUES ('tom', 'tom1', 32);
      INSERT INTO "Users" (login, password, age) VALUES ('julia', 'julia1', 25);
      INSERT INTO "Users" (login, password, age) VALUES ('george', 'george1', 21);
    `);
  },

  down: queryInterface => {
    return queryInterface.sequelize.query(`
      DELETE FROM "Users";
    `);
  }
};
