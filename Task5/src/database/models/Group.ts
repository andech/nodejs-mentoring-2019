import Sequelize, { Model } from "sequelize";
import uuid from "uuid";

export default class GroupModel extends Model {
  static init(sequelize: Express.Application) {
    super.init(
      {
        id: {
          type: Sequelize.UUID,
          defaultValue: uuid(),
          primaryKey: true
        },
        name: Sequelize.STRING,
        permissions: Sequelize.ARRAY(
          Sequelize.ENUM("READ", "WRITE", "DELETE", "SHARE", "UPLOAD_FILES")
        )
      },
      { sequelize, modelName: "Group", timestamps: false }
    );
  }
}
