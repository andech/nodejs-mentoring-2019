import express from "express";
import dbLoader from "./dbLoader";
import appLoader from "./appLoader";
import loggerLoader from "./loggerLoader";
import "../services/Users.service";
import "../services/Groups.service";
import "../controllers/Users.controller";
import "../controllers/Groups.controller";

export default async ({ app }: { app: express.Application }) => {
  loggerLoader();
  await dbLoader();
  return appLoader({ app });
};
