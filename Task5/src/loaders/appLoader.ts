import express, { Request, Response } from "express";
import { loggers } from "winston";
import usersRouter from "../api/routes/users.router";
import groupsRouter from "../api/routes/groups.router";

export default async ({ app }: { app: express.Application }) => {
  const logger = loggers.get("logger");

  app.use(express.json());

  app.use("/users", usersRouter);
  app.use("/groups", groupsRouter);

  app.use("*", (req: Request, res: Response) =>
    res.json({ message: "Endpoint doesn't exist" })
  );

  app.use((err: Error, req: Request, res: Response, next: Function) => {
    res.status(500).json({ error: err.message });
    logger.error(err.message);
    next();
  });

  process
    .on("unhandledRejection", error => {
      logger.error(error);
    })
    .on("uncaughtException", error => {
      logger.error(error);
      process.exit(1);
    });
};
