import {
  Router,
  Request,
  Response,
  NextFunction,
  RequestHandler
} from "express";
import EntitiesController from "../../../controllers/Entities.controller";

export default function addEntitiesRoutes(
  req: Request,
  res: Response,
  next: NextFunction,
  router: Router,
  controller: EntitiesController,
  validator: RequestHandler
) {
  return router
    .get("/", () => controller.findAll(req, res, next))
    .get("/:id", () => controller.findById(req, res, next))
    .post("/", validator, () => controller.create(req, res, next))
    .put("/", validator, () => controller.update(req, res, next))
    .delete("/:id", () => controller.delete(req, res, next));
}
