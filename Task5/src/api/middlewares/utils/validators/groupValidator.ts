import Joi from "@hapi/joi";

export default Joi.object({
  id: Joi.string(),
  name: Joi.string().required(),
  permissions: Joi.array()
    .items(Joi.string())
    .required()
});
