import { Sequelize } from "sequelize";
import { Container } from "typedi";
import config from "../config";
import models from "../database/models";
import Group from "../database/models/Group";
import User from "../database/models/User";

export default async () => {
  const sequelize = new Sequelize(
    config.db.database,
    config.db.username,
    config.db.password,
    { host: config.db.host, dialect: config.db.dialect }
  );
  try {
    Object.values(models).forEach(model => {
      model.init(sequelize);
      Container.set(model.name, model);
    });

    Group.belongsToMany(User, { through: "UserGroup", timestamps: false });
    User.belongsToMany(Group, { through: "UserGroup", timestamps: false });

    await sequelize.authenticate();
    // await sequelize.sync({ force: true });
  } catch (error) {
    console.log(error.message);
  }

  Container.set("db", sequelize);
};
