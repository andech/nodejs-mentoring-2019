import express, { Request, Response } from "express";
import usersRouter from "../api/routes/users.router";
import groupsRouter from "../api/routes/groups.router";

export default async ({ app }: { app: express.Application }) => {
  app.use(express.json());

  app.use("/users", usersRouter);
  app.use("/groups", groupsRouter);

  app.use("*", (req: Request, res: Response) =>
    res.json({ message: "Endpoint doesn't exist" })
  );

  app.use((err: Error, req: Request, res: Response, next: Function) => {
    res.status(500).json({ error: err.message });
    next();
  });
};
