import Sequelize, { Model } from "sequelize";
import uuid from "uuid";

export default class UserModel extends Model {
  static init(sequelize: Express.Application) {
    super.init(
      {
        id: {
          type: Sequelize.UUID,
          defaultValue: uuid(),
          primaryKey: true
        },
        login: Sequelize.STRING,
        password: Sequelize.STRING,
        age: Sequelize.SMALLINT
      },
      { sequelize, modelName: "User", timestamps: false }
    );
  }
}
