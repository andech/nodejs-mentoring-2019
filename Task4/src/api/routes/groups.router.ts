import { Container } from "typedi";
import { NextFunction, Request, Response, Router } from "express";
import addEntitiesRoutes from "./utils/addEntitiesRoutes";
import GroupsController from "../../controllers/Groups.controller";
import validator from "../middlewares/groupValidation";

export default function GroupsRouter(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const controller: GroupsController = Container.get("GroupsController");
  const router = Router().post(
    "/:id/addUsers",
    controller.addUsers.bind(controller)
  );
  const entityRouter = addEntitiesRoutes(
    req,
    res,
    next,
    router,
    controller,
    validator
  );

  return entityRouter(req, res, next);
}
