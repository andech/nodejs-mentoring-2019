import { Container } from "typedi";
import { NextFunction, Request, Response, Router } from "express";
import addEntitiesRoutes from "./utils/addEntitiesRoutes";
import UsersController from "../../controllers/Users.controller";
import validator from "../middlewares/userValidation";

export default function UsersRouter(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const controller: UsersController = Container.get("UsersController");
  const router = Router().get(
    "/autosuggest",
    controller.getAutoSuggestUsers.bind(controller)
  );
  const entityRouter = addEntitiesRoutes(
    req,
    res,
    next,
    router,
    controller,
    validator
  );

  return entityRouter(req, res, next);
}
