import { Inject, Service } from "typedi";
import { NextFunction, Request, Response } from "express";
import GroupsService from "../services/Groups.service";
import EntitiesController from "./Entities.controller";

@Service("GroupsController")
export default class GroupsController extends EntitiesController {
  @Inject("GroupsService")
  service: GroupsService;

  async addUsers(
    { params: { id }, body: { userIds } }: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const result = await this.service.addUsers(id, userIds);
      return res.json(result);
    } catch (error) {
      return next(error);
    }
  }
}
