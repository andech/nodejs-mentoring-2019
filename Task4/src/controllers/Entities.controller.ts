import { NextFunction, Request, Response } from "express";
import EntitiesService from "../services/Entities.service";

export default abstract class EntitiesController {
  service: EntitiesService;

  async findAll(req: Request, res: Response, next: NextFunction) {
    try {
      const entities = await this.service.findAll();
      return res.json(entities);
    } catch (error) {
      return next(error);
    }
  }

  async findById(
    { params: { id } }: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const entities = await this.service.findById(id);
      return res.json(entities);
    } catch (error) {
      return next(error);
    }
  }

  async create({ body }: Request, res: Response, next: NextFunction) {
    try {
      const createdEntity = await this.service.create(body);
      return res.json(createdEntity);
    } catch (error) {
      return next(error);
    }
  }

  async update({ body }: Request, res: Response, next: NextFunction) {
    try {
      const updatedEntity = await this.service.update(body);
      return res.json(updatedEntity);
    } catch (error) {
      return next(error);
    }
  }

  async delete({ params: { id } }: Request, res: Response, next: NextFunction) {
    try {
      const changedEntities = await this.service.delete(id);
      return res.json(changedEntities);
    } catch (error) {
      return next(error);
    }
  }
}
