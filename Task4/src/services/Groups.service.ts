import Sequelize from "sequelize";
import { Container, Inject, Service } from "typedi";
import GroupModel from "../database/models/Group";
import EntitiesService from "./Entities.service";

@Service("GroupsService")
export default class GroupsService extends EntitiesService {
  @Inject("Group")
  model: GroupModel;

  async addUsers(id: string, userIds: string[]) {
    const db: Sequelize.Sequelize = Container.get("db");
    const userGroupModel = db.models.UserGroup;
    const rowsToAdd = userIds.map(userId => ({ GroupId: id, UserId: userId }));
    await db.transaction(t => {
      const requests = rowsToAdd.map(rowToAdd =>
        userGroupModel.create(rowToAdd, { transaction: t })
      );
      return Promise.all(requests);
    });
    return this.model.findByPk(id, {
      include: Container.get("User"),
      plain: true
    });
  }

  async findAll() {
    const foundGroups = await this.model.findAll({
      include: Container.get("User")
    });
    return foundGroups.map((group: GroupModel) => group.get({ plain: true }));
  }

  async findById(id: string) {
    const foundGroup = await this.model.findByPk(id, {
      include: Container.get("User")
    });
    return foundGroup || {};
  }
}
