import { Container } from "typedi";
import express, { Router } from "express";
import addEntitiesRoutes from "./utils/addEntitiesRoutes";
import UsersController from "../../controllers/Users.controller";
import validator from "../middlewares/userValidation";
import auth from "../middlewares/auth";

export default (router: express.Router) => {
  const controller: UsersController = Container.get("UsersController");
  const usersRouter = Router().get(
    "/autosuggest",
    auth,
    controller.getAutoSuggestUsers.bind(controller)
  );
  addEntitiesRoutes(usersRouter, controller, validator);

  return router.use("/users", usersRouter);
};
