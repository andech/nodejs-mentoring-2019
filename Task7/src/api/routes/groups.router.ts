import { Container } from "typedi";
import express, { Router } from "express";
import addEntitiesRoutes from "./utils/addEntitiesRoutes";
import GroupsController from "../../controllers/Groups.controller";
import validator from "../middlewares/groupValidation";
import auth from "../middlewares/auth";

export default (router: express.Router) => {
  const controller: GroupsController = Container.get("GroupsController");
  const groupsRouter = Router().post(
    "/:id/addUsers",
    auth,
    controller.addUsers.bind(controller)
  );
  addEntitiesRoutes(groupsRouter, controller, validator);

  return router.use("/groups", groupsRouter);
};
