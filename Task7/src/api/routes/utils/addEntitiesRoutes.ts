import { Router, RequestHandler } from "express";
import EntitiesController from "../../../controllers/Entities.controller";
import auth from "../../middlewares/auth";

export default function addEntitiesRoutes(
  router: Router,
  controller: EntitiesController,
  validator: RequestHandler
) {
  return router
    .get("/", auth, controller.findAll.bind(controller))
    .get("/:id", auth, controller.findById.bind(controller))
    .post("/", auth, validator, controller.create.bind(controller))
    .put("/", auth, validator, controller.update.bind(controller))
    .delete("/:id", auth, controller.delete.bind(controller));
}
