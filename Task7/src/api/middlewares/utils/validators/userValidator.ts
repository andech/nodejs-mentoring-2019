import Joi from "@hapi/joi";

export default Joi.object({
  userId: Joi.string(),
  login: Joi.string().required(),
  password: Joi.string()
    .pattern(new RegExp("([a-zA-Z]+)([0-9]+)"))
    .required(),
  age: Joi.number()
    .min(4)
    .max(130)
    .required()
});
