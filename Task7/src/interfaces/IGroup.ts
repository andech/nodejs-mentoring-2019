export default interface IGroup {
  id: string;
  name: string;
  permissions: Array<Permission>;
}
