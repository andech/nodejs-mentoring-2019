export interface ITokensPair {
  token: string;
  refreshToken: string;
}
