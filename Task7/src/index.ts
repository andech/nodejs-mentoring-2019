import "reflect-metadata";
import express from "express";
import { loggers } from "winston";
import loaders from "./loaders";
import config from "./config";

async function startServer() {
  const app = express();
  const { port } = config.server;

  try {
    await loaders({ app });
  } catch (error) {
    const logger = loggers.get("logger");
    logger.error(error.message);
  }
  const logger = loggers.get("logger");

  app.listen(port, () => logger.info(`Server started on port ${port}`));
}

startServer();
