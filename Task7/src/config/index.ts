import dotenv from "dotenv";

dotenv.config({ path: "./src/config/.env" });

const db = require("./database").development;

export default {
  server: {
    port: process.env.PORT || 3000
  },
  db,
  jwtSecret: process.env.JWT_SECRET || "",
  jwtLifetime: "10m"
};
