import { Request, Response, NextFunction } from "express";
import { Inject, Service } from "typedi";
import AuthService from "../services/Auth.service";
import { ITokensPair } from "../interfaces/ITokensPair";

@Service("AuthController")
export default class AuthController {
  @Inject("AuthService")
  service: AuthService;

  async signIn({ body: { login, password } }: Request, res: Response) {
    try {
      const tokens: ITokensPair = await this.service.signIn(login, password);
      return res.json(tokens);
    } catch (error) {
      return res.status(403).json({ error: error.message });
    }
  }

  async signOut(
    { body: { userId } }: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      await this.service.signOut(userId);
      return res.json({ message: "User successfully signed out" });
    } catch (error) {
      return next(error);
    }
  }

  async refresh(
    { body: { refreshToken } }: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const tokens = await this.service.refresh(refreshToken);
      return res.json(tokens);
    } catch (error) {
      return next(error);
    }
  }
}
