import { NextFunction, Request, Response } from "express";
import { Inject, Service } from "typedi";
import UsersService from "../services/Users.service";
import EntitiesController from "./Entities.controller";
import LogController from "../decorators/logController.decorator";

@LogController
@Service("UsersController")
export default class UsersController extends EntitiesController {
  @Inject("UsersService")
  service: UsersService;

  async getAutoSuggestUsers(
    { query: { loginSubstring, limit } }: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const suggestedUsers = await this.service.getAutoSuggestUsers(
        loginSubstring,
        +limit || 5
      );
      return res.json(suggestedUsers);
    } catch (error) {
      return next(error);
    }
  }
}
