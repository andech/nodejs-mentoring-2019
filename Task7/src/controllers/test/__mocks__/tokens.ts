import { ITokensPair } from "../../../interfaces/ITokensPair";

export default {
  token: "token",
  refreshToken: "refresh"
} as ITokensPair;
