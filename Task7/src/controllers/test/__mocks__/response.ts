import { Response } from "express";

export default ({
  json() {
    return this;
  },
  status() {
    return this;
  }
} as unknown) as Response;
