import { Request } from "express";
import mockResponse from "./__mocks__/response";
import mockNext from "./__mocks__/next";
import mockError from "./__mocks__/error";
import EntitiesController from "./__mocks__/Entities.controller";
import EntitiesService from "../../services/__mocks__/Entities.service";

jest.mock("../../services/__mocks__/Entities.service");

describe("EntitiesController", () => {
  const entitiesController: EntitiesController = new EntitiesController();
  entitiesController.service = new EntitiesService();

  beforeEach(() => {
    (EntitiesService as jest.Mock).mockClear();
    jest.spyOn(mockResponse, "json");
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("Instance should be created", () => {
    expect(entitiesController).toBeInstanceOf(EntitiesController);
  });

  describe("findAll", () => {
    const mockRequest = {} as Request;
    const entities: any[] = [];

    it("Should successfully response with all entities", async () => {
      jest
        .spyOn(entitiesController.service, "findAll")
        .mockResolvedValue(entities);

      await entitiesController.findAll(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.findAll).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.findAll).toHaveBeenCalledWith();
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(entities);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(entitiesController.service, "findAll")
        .mockRejectedValue(mockError);

      await entitiesController.findAll(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.findAll).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.findAll).toHaveBeenCalledWith();
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });

  describe("findById", () => {
    const id = 1;
    const mockRequest = ({ params: { id } } as unknown) as Request;
    const entity = { id: 1, value: "test" };

    it("Should successfully response with found entity or with {}, if it not exists", async () => {
      jest
        .spyOn(entitiesController.service, "findById")
        .mockResolvedValue(entity);

      await entitiesController.findById(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.findById).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.findById).toHaveBeenCalledWith(id);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(entity);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(entitiesController.service, "findById")
        .mockRejectedValue(mockError);

      await entitiesController.findById(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.findById).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.findById).toHaveBeenCalledWith(id);
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });

  describe("create", () => {
    const body = { entity: "test" };
    const mockRequest = { body } as Request;

    it("Should successfully response with created entity", async () => {
      jest.spyOn(entitiesController.service, "create").mockResolvedValue(body);

      await entitiesController.create(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.create).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.create).toHaveBeenCalledWith(body);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(body);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(entitiesController.service, "create")
        .mockRejectedValue(mockError);

      await entitiesController.create(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.create).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.create).toHaveBeenCalledWith(body);
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });

  describe("update", () => {
    const body = { entity: "test" };
    const mockRequest = { body } as Request;

    it("Should successfully response with updated entity", async () => {
      jest.spyOn(entitiesController.service, "update").mockResolvedValue(body);

      await entitiesController.update(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.update).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.update).toHaveBeenCalledWith(body);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(body);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(entitiesController.service, "update")
        .mockRejectedValue(mockError);

      await entitiesController.update(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.update).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.update).toHaveBeenCalledWith(body);
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });

  describe("delete", () => {
    const id = 1;
    const mockRequest = ({ params: { id } } as unknown) as Request;
    const entities = [{ id: 1, value: "test" }];

    it("Should successfully response with refreshed list of all entities", async () => {
      jest
        .spyOn(entitiesController.service, "delete")
        .mockResolvedValue(entities);

      await entitiesController.delete(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.delete).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.delete).toHaveBeenCalledWith(id);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(entities);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(entitiesController.service, "delete")
        .mockRejectedValue(mockError);

      await entitiesController.delete(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(entitiesController.service.delete).toHaveBeenCalledTimes(1);
      expect(entitiesController.service.delete).toHaveBeenCalledWith(id);
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });
});
