import { Request } from "express";
import mockResponse from "./__mocks__/response";
import mockNext from "./__mocks__/next";
import mockError from "./__mocks__/error";
import UsersService from "../../services/Users.service";
import UsersController from "../Users.controller";

jest.mock("../../services/Users.service");

describe("UsersController", () => {
  const usersController: UsersController = new UsersController();
  usersController.service = new UsersService();

  beforeEach(() => {
    (UsersService as jest.Mock).mockClear();
    jest.spyOn(mockResponse, "json");
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("Instance should be created", () => {
    expect(usersController).toBeInstanceOf(UsersController);
  });

  describe("getAutoSuggestUsers", () => {
    const loginSubstring = "test";
    const limit = 2;
    const mockRequest = { query: { loginSubstring, limit } } as Request;
    const users: any[] = [{ user: "user" }];

    it("Should successfully response with suggested user entities list or with []", async () => {
      jest
        .spyOn(usersController.service, "getAutoSuggestUsers")
        .mockResolvedValue(users);

      await usersController.getAutoSuggestUsers(
        mockRequest,
        mockResponse,
        mockNext
      );

      expect.assertions(4);
      expect(usersController.service.getAutoSuggestUsers).toHaveBeenCalledTimes(
        1
      );
      expect(usersController.service.getAutoSuggestUsers).toHaveBeenCalledWith(
        loginSubstring,
        limit
      );
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(users);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(usersController.service, "getAutoSuggestUsers")
        .mockRejectedValue(mockError);

      await usersController.getAutoSuggestUsers(
        mockRequest,
        mockResponse,
        mockNext
      );

      expect.assertions(4);
      expect(usersController.service.getAutoSuggestUsers).toHaveBeenCalledTimes(
        1
      );
      expect(usersController.service.getAutoSuggestUsers).toHaveBeenCalledWith(
        loginSubstring,
        limit
      );
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });
});
