import { Request } from "express";
import mockResponse from "./__mocks__/response";
import mockNext from "./__mocks__/next";
import mockError from "./__mocks__/error";
import GroupsController from "../Groups.controller";
import GroupsService from "../../services/Groups.service";

jest.mock("../../services/Groups.service");

describe("GroupsController", () => {
  const groupsController: GroupsController = new GroupsController();
  groupsController.service = new GroupsService();

  beforeEach(() => {
    (GroupsService as jest.Mock).mockClear();
    jest.spyOn(mockResponse, "json");
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("Instance should be created", () => {
    expect(groupsController).toBeInstanceOf(GroupsController);
  });

  describe("addUsers", () => {
    const id = 1;
    const userIds = [1, 2, 3];
    const mockRequest = ({
      params: { id },
      body: { userIds }
    } as unknown) as Request;
    const group = {};

    it("Should successfully response with refreshed group data", async () => {
      jest.spyOn(groupsController.service, "addUsers").mockResolvedValue(group);

      await groupsController.addUsers(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(groupsController.service.addUsers).toHaveBeenCalledTimes(1);
      expect(groupsController.service.addUsers).toHaveBeenCalledWith(
        id,
        userIds
      );
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(group);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(groupsController.service, "addUsers")
        .mockRejectedValue(mockError);

      await groupsController.addUsers(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(groupsController.service.addUsers).toHaveBeenCalledTimes(1);
      expect(groupsController.service.addUsers).toHaveBeenCalledWith(
        id,
        userIds
      );
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });
});
