import { Request } from "express";
import AuthController from "../Auth.controller";
import AuthService from "../../services/Auth.service";
import mockResponse from "./__mocks__/response";
import mockTokens from "./__mocks__/tokens";
import mockNext from "./__mocks__/next";
import mockError from "./__mocks__/error";

jest.mock("../../services/Auth.service");

describe("AuthController", () => {
  const authController: AuthController = new AuthController();
  authController.service = new AuthService();

  beforeEach(() => {
    (AuthService as jest.Mock).mockClear();
    jest.spyOn(mockResponse, "json");
    jest.spyOn(mockResponse, "status");
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("Instance should be created", () => {
    expect(authController).toBeInstanceOf(AuthController);
  });

  describe("signIn", () => {
    const login = "test";
    const password = "test";
    const mockRequest = {
      body: { login, password }
    } as Request;

    it("Should successfully response with generated tokens", async () => {
      jest
        .spyOn(authController.service, "signIn")
        .mockResolvedValue(mockTokens);

      await authController.signIn(mockRequest, mockResponse);

      expect.assertions(4);
      expect(authController.service.signIn).toHaveBeenCalledTimes(1);
      expect(authController.service.signIn).toHaveBeenCalledWith(
        login,
        password
      );
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(mockTokens);
    });

    it("Should failed with response status 403 and error message", async () => {
      jest.spyOn(authController.service, "signIn").mockRejectedValue(mockError);

      await authController.signIn(mockRequest, mockResponse);

      expect.assertions(6);
      expect(authController.service.signIn).toHaveBeenCalledTimes(1);
      expect(authController.service.signIn).toHaveBeenCalledWith(
        login,
        password
      );
      expect(mockResponse.status).toHaveBeenCalledTimes(1);
      expect(mockResponse.status).toHaveBeenCalledWith(403);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith({
        error: mockError.message
      });
    });
  });

  describe("signOut", () => {
    const userId = "user";
    const mockRequest = {
      body: { userId }
    } as Request;

    it("Should successfully response with message", async () => {
      const message = { message: "User successfully signed out" };
      jest.spyOn(authController.service, "signOut").mockResolvedValue();

      await authController.signOut(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(authController.service.signOut).toHaveBeenCalledTimes(1);
      expect(authController.service.signOut).toHaveBeenCalledWith(userId);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(message);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(authController.service, "signOut")
        .mockRejectedValue(mockError);

      await authController.signOut(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(authController.service.signOut).toHaveBeenCalledTimes(1);
      expect(authController.service.signOut).toHaveBeenCalledWith(userId);
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });

  describe("refresh", () => {
    const refreshToken = "refreshToken";
    const mockRequest = {
      body: { refreshToken }
    } as Request;

    it("Should successfully response with new pair of tokens", async () => {
      jest
        .spyOn(authController.service, "refresh")
        .mockResolvedValue(mockTokens);

      await authController.refresh(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(authController.service.refresh).toHaveBeenCalledTimes(1);
      expect(authController.service.refresh).toHaveBeenCalledWith(refreshToken);
      expect(mockResponse.json).toHaveBeenCalledTimes(1);
      expect(mockResponse.json).toHaveBeenCalledWith(mockTokens);
    });

    it("Should failed and execute 'next' function with error", async () => {
      jest
        .spyOn(authController.service, "refresh")
        .mockRejectedValue(mockError);

      await authController.refresh(mockRequest, mockResponse, mockNext);

      expect.assertions(4);
      expect(authController.service.refresh).toHaveBeenCalledTimes(1);
      expect(authController.service.refresh).toHaveBeenCalledWith(refreshToken);
      expect(mockNext).toHaveBeenCalledTimes(1);
      expect(mockNext).toHaveBeenCalledWith(mockError);
    });
  });
});
