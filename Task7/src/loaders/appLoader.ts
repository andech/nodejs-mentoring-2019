import express, { Request, Response } from "express";
import cors from "cors";
import { loggers } from "winston";
import routes from "../api/routes";

export default ({ app }: { app: express.Application }) => {
  const logger = loggers.get("logger");

  app.use(cors());
  app.use(express.json());

  app.use("/", routes());

  app.use("*", (req: Request, res: Response) =>
    res.json({ message: "Endpoint doesn't exist" })
  );

  app.use((err: Error, req: Request, res: Response, next: Function) => {
    res.status(500).json({ error: err.message });
    logger.error(err.message);
    next();
  });

  process
    .on("unhandledRejection", error => {
      logger.error(error);
    })
    .on("uncaughtException", error => {
      logger.error(error);
      process.exit(1);
    });

  return app;
};
