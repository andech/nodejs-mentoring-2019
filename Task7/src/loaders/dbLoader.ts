import { Sequelize } from "sequelize";
import { Container } from "typedi";
import { loggers } from "winston";
import config from "../config";
import models from "../database/models";
import Group from "../database/models/Group";
import User from "../database/models/User";
import RefreshToken from "../database/models/RefreshToken";

export default async () => {
  const logger = loggers.get("logger");
  const sequelize = new Sequelize(
    config.db.database,
    config.db.username,
    config.db.password,
    { host: config.db.host, dialect: config.db.dialect }
  );
  try {
    Object.values(models).forEach(model => {
      model.init(sequelize);
      Container.set(model.name, model);
    });
    logger.info("Database models initialized");

    Group.belongsToMany(User, { through: "UserGroup", timestamps: false });
    User.belongsToMany(Group, { through: "UserGroup", timestamps: false });
    User.hasMany(RefreshToken);

    await sequelize.authenticate();
    // await sequelize.sync({ force: true });
  } catch (error) {
    logger.error(`Database connection failed: ${error.message}`);
  }

  Container.set("db", sequelize);
  logger.info("Database connection established");
};
