import { loggers, format, transports } from "winston";

export default () => {
  loggers.add("logger", {
    level: "info",
    format: format.combine(format.colorize(), format.cli()),
    transports: [new transports.Console()]
  });
};
