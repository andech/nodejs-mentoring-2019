import getLogger from "./utils/getLogger";

export default function LogService(target: Function) {
  const descriptors = Object.getOwnPropertyDescriptors(target.prototype);
  Object.entries(descriptors).forEach(([name, descriptor]) => {
    if (name === "constructor" || typeof descriptor.value !== "function") {
      return;
    }

    const isAsync = target.prototype[name].toString().startsWith("async");

    target.prototype[name] = isAsync
      ? async function decoratedMethod(...args: Array<any>) {
          const logger = getLogger();
          logger.debug(
            `${target.name}.${name}() called with args: ${JSON.stringify(args)}`
          );

          logger.profile(`${target.name}.${name}()`);
          const result = await descriptors[name].value.call(this, ...args);
          logger.profile(`${target.name}.${name}()`);

          return result;
        }
      : function decoratedMethod(...args: Array<any>) {
          const logger = getLogger();
          logger.debug(
            `${target.name}.${name}() called with args: ${JSON.stringify(args)}`
          );

          logger.profile(`${target.name}.${name}()`);
          const result = descriptors[name].value.call(this, ...args);
          logger.profile(`${target.name}.${name}()`);

          return result;
        };
  });
}
