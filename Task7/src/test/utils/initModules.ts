import { Container } from "typedi";
import AuthService from "../../services/Auth.service";
import UsersService from "../../services/Users.service";
import GroupsService from "../../services/Groups.service";
import AuthController from "../../controllers/Auth.controller";
import UsersController from "../../controllers/Users.controller";
import GroupsController from "../../controllers/Groups.controller";

export default () => {
  Container.set("AuthService", new AuthService());
  Container.set("UsersService", new UsersService());
  Container.set("GroupsService", new GroupsService());
  Container.set("AuthController", new AuthController());
  Container.set("UsersController", new UsersController());
  Container.set("GroupsController", new GroupsController());
};
