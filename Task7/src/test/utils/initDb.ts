import { Container } from "typedi";
import User from "../../database/models/__mocks__/User";
import RefreshToken from "../../database/models/__mocks__/RefreshToken";

export default () => {
  Container.set("User", new User());
  Container.set("Group", {});
  Container.set("RefreshToken", new RefreshToken());
};
