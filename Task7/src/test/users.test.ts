import request from "supertest";
import express from "express";
import apploader from "../loaders/appLoader";
import initDb from "./utils/initDb";
import initModules from "./utils/initModules";

describe("Integration for '/users'", () => {
  initDb();
  const app = apploader({ app: express() });
  initModules();

  async function loginUser(login: string, password: string) {
    const {
      body: { token }
    } = await request(app)
      .post("/auth/signin")
      .send({ login, password });
    return `Bearer ${token}`;
  }

  describe("GET /users", () => {
    it("Should successfully response with list of all users", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);

      const response = await request(app)
        .get("/users")
        .set("Authorization", token);

      expect(response.status).toEqual(200);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 403, because of unknown user", async () => {
      const login = "admin";
      const password = "test";
      const token = await loginUser(login, password);

      const response = await request(app)
        .get("/users")
        .set("Authorization", token);

      expect(response.status).toEqual(403);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 401, because of unauthorized user", async () => {
      const response = await request(app).get("/users");

      expect(response.status).toEqual(401);
      expect(response.type).toEqual("application/json");
    });
  });

  describe("POST /users", () => {
    it("Should successfully response with created user data", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);
      const user = {
        login: "user1",
        password: "user1",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(200);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 500, because password is absent", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);
      const user = {
        login: "user",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(500);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 500, because login is absent", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);
      const user = {
        password: "user1",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(500);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 500, because password doesn't have digits", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);
      const user = {
        login: "user",
        password: "user",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(500);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 500, because user with this login already exist", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);
      const user = {
        login: "admin",
        password: "user1",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(500);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 500, because body is empty", async () => {
      const login = "admin";
      const password = "admin";
      const token = await loginUser(login, password);
      const user = {};

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(500);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 403, because of unknown user", async () => {
      const login = "admin";
      const password = "user";
      const token = await loginUser(login, password);
      const user = {
        login: "user",
        password: "user1",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user)
        .set("Authorization", token);

      expect(response.status).toEqual(403);
      expect(response.type).toEqual("application/json");
    });

    it("Should failed with response status 401, because of unauthorized user", async () => {
      const user = {
        login: "user",
        password: "user1",
        age: 23
      };

      const response = await request(app)
        .post("/users")
        .send(user);

      expect(response.status).toEqual(401);
      expect(response.type).toEqual("application/json");
    });
  });
});
