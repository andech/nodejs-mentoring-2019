const bcrypt = require("bcrypt");

export default class User {
  private users = [
    {
      id: "1",
      login: "admin",
      password: bcrypt.hashSync("admin", 10),
      age: 23
    }
  ];

  async findOne({ where: { login } }) {
    return this.users.find(user => user.login === login);
  }

  async findAll() {
    return [
      {
        get() {
          return this.users;
        }
      }
    ];
  }

  async create(body) {
    if (this.users.some(user => user.login === body.login)) {
      throw new Error("User already exists");
    }
    return {
      get() {
        return body;
      }
    };
  }
}
