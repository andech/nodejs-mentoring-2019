import Sequelize, {
  InstanceDestroyOptions,
  InstanceUpdateOptions,
  Model
} from "sequelize";
import LogService from "../decorators/logService.decorator";

@LogService
export default abstract class EntitiesService {
  abstract model: Sequelize.Model;

  async findAll() {
    const foundEntities = await this.model.findAll();
    return foundEntities.map((entity: Sequelize.Model) =>
      entity.get({ plain: true })
    );
  }

  async create(entity: Object) {
    const createdEntity = await this.model.create(entity);
    return createdEntity.get({ plain: true });
  }

  async findById(id: string) {
    const foundEntity = await this.model.findByPk(id);
    return foundEntity || {};
  }

  async update(entity: { id: string }) {
    const [, updatedEntity]: [
      number,
      Model<any, any>[]
    ] = (await this.model.update(entity, {
      where: { id: entity.id },
      returning: true
    } as InstanceUpdateOptions)) as any;
    if (!updatedEntity.length) {
      throw new Error("There is no user with this id");
    }
    return updatedEntity[0].get({ plain: true });
  }

  async delete(id: string) {
    await this.model.destroy({ where: { id } } as InstanceDestroyOptions);
    return this.findAll();
  }
}
