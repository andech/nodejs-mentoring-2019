export interface IUser {
  id: string;
  login: string;
  password: string;
  age: number;
  isDelete: boolean;
}
