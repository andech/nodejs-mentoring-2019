import { Request, Response } from "express";
import { UsersService } from "../services/Users.service";
import { IUser } from "../interfaces/IUser";

export class UsersController {
  static getAllUsers(req: Request, res: Response): void {
    const users: IUser[] = UsersService.getAllUsers();
    res.json(users);
  }

  static getUserById({ params: { id } }: Request, res: Response): void {
    const user: IUser | undefined = UsersService.getUser(id);
    res.json(user || {});
  }

  static createUser({ body }: Request, res: Response): void {
    const createdUser: IUser = UsersService.createUser(body);
    res.json(createdUser);
  }

  static updateUser({ body }: Request, res: Response): void {
    const updatedUser: IUser = UsersService.updateUser(body);
    res.json(updatedUser);
  }

  static removeUser({ params: { id } }: Request, res: Response) {
    const changedUsers: IUser[] = UsersService.removeUser(id);
    res.json(changedUsers);
  }

  static getAutoSuggestUsers(
    { query: { loginSubstring, limit } }: Request,
    res: Response
  ): void {
    const suggestedUsers = UsersService.getAutoSuggestUsers(
      loginSubstring,
      limit
    );
    res.json(suggestedUsers);
  }
}
