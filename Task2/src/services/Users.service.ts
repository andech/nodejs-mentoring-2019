import db from "../mocks/usersMock.json";
import { IUser } from "../interfaces/IUser";

export class UsersService {
  private static db: IUser[] = db;

  static getAllUsers(): IUser[] {
    return this.db;
  }

  static getUser(id: string): IUser | undefined {
    return this.db.find(user => user.id === id);
  }

  static createUser(user: IUser): IUser {
    this.db = [...this.db, user];
    return user;
  }

  static updateUser(userToUpdate: IUser): IUser {
    const { id } = userToUpdate;
    this.db = this.db.map(user => (user.id === id ? userToUpdate : user));
    return userToUpdate;
  }

  static removeUser(id: string): IUser[] {
    return (this.db = this.db.map(user =>
      user.id === id ? { ...user, isDelete: true } : user
    ));
  }

  static getAutoSuggestUsers(loginSubstring: string, limit: string): IUser[] {
    return this.db
      .filter((user: IUser) => user.login.includes(loginSubstring))
      .sort((a: IUser, b: IUser) => {
        if (a.login > b.login) {
          return 1;
        }
        if (a.login < b.login) {
          return -1;
        }
        return 0;
      })
      .slice(0, +limit);
  }
}
