import { Router } from "express";
import { UsersController } from "../controllers/Users.controller";
import { userValidation } from "../middlewares/userValidation";

export const usersRouter = Router()
  .get("/", UsersController.getAllUsers)
  .get("/autosuggest", UsersController.getAutoSuggestUsers)
  .get("/:id", UsersController.getUserById)
  .post("/", userValidation, UsersController.createUser)
  .put("/", userValidation, UsersController.updateUser)
  .delete("/:id", UsersController.removeUser);
