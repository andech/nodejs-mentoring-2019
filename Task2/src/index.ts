import express, { Response, Request } from "express";
import { usersRouter } from "./routes/users.router";

const app = express();
const port = 3001;

app.use(express.json());

app.use("/users", usersRouter);

app.use("*", (req: Request, res: Response) =>
  res.json({ message: "Endpoint doesn't exist" })
);

app.use((err: Error, req: Request, res: Response, next: Function) => {
  res.status(500).json({ error: err.message });
});

app.listen(port, () => console.log(`Server started on port ${port}`));
