import readline from 'readline';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: '',
    terminal: false
});
rl.prompt();
rl.on('line', (line) => {
    const output = line.split(' ').map(word => word.split('').reverse().join('')).reverse().join(' ');
    console.log(`${output}\n`);
    rl.prompt();
}).on('close', () => {
    process.exit(0);
});