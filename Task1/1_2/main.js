const fs = require('fs');
const path = require('path');
const csv = require('csvtojson');

const csvFilePath = path.resolve(__dirname, 'csv', 'data.csv');
const txtFilePath = path.resolve(__dirname, 'txt', 'data.txt');
const readStream = csv().fromFile(csvFilePath);
const writeStream = fs.createWriteStream(txtFilePath);

readStream.on('data', chunk => {
    console.log(chunk.toString());
    writeStream.write(chunk);
});

writeStream.on('error', error => console.log(error));
readStream.on('done', error => console.log(error || 'Parsing successfully completed!'));