module.exports = {
  up: queryInterface => {
    return queryInterface.sequelize.query(
      `DROP FUNCTION IF EXISTS get_autosuggested_users;
       CREATE OR REPLACE FUNCTION get_autosuggested_users(substring_text VARCHAR, limit_output INT)
       RETURNS SETOF "Users" AS 
       $BODY$ 
          SELECT * FROM "Users" WHERE "login" LIKE '%' || substring_text || '%' LIMIT limit_output;
       $BODY$ LANGUAGE SQL;`
    );
  },

  down: queryInterface => {
    return queryInterface.sequelize.query(
      `DROP FUNCTION IF EXISTS get_autosuggested_users;`
    );
  }
};
