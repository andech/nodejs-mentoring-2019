import Sequelize, { Model } from "sequelize";

export default class RefreshTokenModel extends Model {
  static init(sequelize: Express.Application) {
    super.init(
      {
        id: {
          type: Sequelize.STRING,
          primaryKey: true
        },
        UserId: Sequelize.UUID
      },
      { sequelize, modelName: "RefreshToken", timestamps: false }
    );
  }
}
