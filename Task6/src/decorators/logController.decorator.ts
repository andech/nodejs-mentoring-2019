import { NextFunction, Request, Response } from "express";
import getLogger from "./utils/getLogger";

export default function LogController(target: Function) {
  const descriptors = Object.getOwnPropertyDescriptors(target.prototype);
  Object.entries(descriptors).forEach(([name, descriptor]) => {
    if (name === "constructor" || typeof descriptor.value !== "function") {
      return;
    }

    const isAsync = target.prototype[name].toString().startsWith("async");

    target.prototype[name] = isAsync
      ? async function(req: Request, res: Response, next: NextFunction) {
          const { params, body } = req;
          const logger = getLogger();
          logger.debug(
            `${target.name}.${name}() called with args: ${JSON.stringify({
              params,
              body
            })}`
          );

          logger.profile(`${target.name}.${name}()`);
          const result = await descriptors[name].value.call(
            this,
            req,
            res,
            next
          );

          if (!result) {
            logger.error(
              `${target.name}.${name}() call with args: ${JSON.stringify({
                params,
                body
              })} failed`
            );
          } else {
            logger.profile(`${target.name}.${name}()`);
          }
          return result;
        }
      : function(req: Request, res: Response, next: NextFunction) {
          const { params, body } = req;
          const logger = getLogger();
          logger.debug(
            `${target.name}.${name}() called with args: ${JSON.stringify({
              params,
              body
            })}`
          );

          logger.profile(`${target.name}.${name}()`);
          const result = descriptors[name].value.call(this, req, res, next);

          if (!result) {
            logger.error(
              `${target.name}.${name}() call with args: ${JSON.stringify({
                params,
                body
              })} failed`
            );
          } else {
            logger.profile(`${target.name}.${name}()`);
          }
          return result;
        };
  });
}
