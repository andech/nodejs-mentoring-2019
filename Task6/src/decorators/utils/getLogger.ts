import { createLogger, format, transports } from "winston";

export default function getLogger() {
  const date = new Date().toISOString();
  const logFormat = format.printf(info =>
    info.durationMs
      ? `${date}-${info.level}: ${info.message} completed in ${info.durationMs}Ms`
      : `${date}-${info.level}: ${info.message}`
  );
  return createLogger({
    level: "debug",
    format: format.combine(format.colorize(), logFormat),
    transports: [
      new transports.Console(),
      new transports.File({
        filename: "error.log",
        dirname: "logs",
        level: "error"
      })
    ]
  });
}
