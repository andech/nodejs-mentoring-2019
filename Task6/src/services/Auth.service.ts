import { Inject, Service } from "typedi";
import bcrypt from "bcrypt";
import uuid from "uuid";
import { sign } from "jsonwebtoken";
import config from "../config";
import LogService from "../decorators/logService.decorator";
import UserModel from "../database/models/User";
import RefreshTokenModel from "../database/models/RefreshToken";

@LogService
@Service("AuthService")
export default class AuthService {
  @Inject("User")
  model: UserModel;

  @Inject("RefreshToken")
  modelRefresh: RefreshTokenModel;

  async signIn(login: string, password: string) {
    const user = await this.model.findOne({
      where: { login }
    });
    if (user) {
      const isValid = await bcrypt.compare(password, user.password);
      if (!isValid) {
        throw new Error("Authorization error");
      }
    }

    return this.issueTokens(user.id);
  }

  async signOut(userId: string) {
    await this.modelRefresh.destroy({ where: { UserId: userId } });
  }

  async refresh(refreshToken: string) {
    const userToken = await this.modelRefresh.findOne({
      where: { id: refreshToken },
      plain: true
    });
    if (!userToken) {
      throw new Error("Invalid refresh token");
    }
    await this.modelRefresh.destroy({ where: { id: refreshToken } });

    return this.issueTokens(userToken.UserId);
  }

  async issueTokens(userId: string) {
    const token = sign({ sub: userId }, config.jwtSecret, {
      expiresIn: config.jwtLifetime
    });
    const refreshToken = uuid();
    await this.modelRefresh.create({
      id: refreshToken,
      UserId: userId
    });

    return {
      token,
      refreshToken
    };
  }
}
