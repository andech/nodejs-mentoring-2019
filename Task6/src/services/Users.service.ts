import { Container, Inject, Service } from "typedi";
import Sequelize, { Op } from "sequelize";
import UserModel from "../database/models/User";
import EntitiesService from "./Entities.service";
import LogService from "../decorators/logService.decorator";

@LogService
@Service("UsersService")
export default class UsersService extends EntitiesService {
  @Inject("User")
  model: UserModel;

  async findAll() {
    const foundEntities = await this.model.findAll({
      include: [Container.get("Group"), Container.get("RefreshToken")]
    });
    return foundEntities.map((entity: Sequelize.Model) =>
      entity.get({ plain: true })
    );
  }

  async findById(id: string) {
    const foundUser = await this.model.findByPk(id, {
      include: Container.get("Group")
    });
    return foundUser || {};
  }

  async getAutoSuggestUsers(loginSubstring: string, limit: number) {
    const foundUsers = await this.model.findAll({
      where: {
        login: {
          [Op.like]: `%${loginSubstring}%`
        }
      },
      order: ["login"],
      limit,
      plain: true
    });

    return foundUsers;
  }
}
