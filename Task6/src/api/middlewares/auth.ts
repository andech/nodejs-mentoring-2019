import { verify } from "jsonwebtoken";
import { NextFunction, Request, Response } from "express";
import config from "../../config";

export default async (req: Request, res: Response, next: NextFunction) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return res.status(401).json({ error: "Unauthorized" });
  }

  const token = authorization.split(" ")[1];
  try {
    const { sub } = await verify(token, config.jwtSecret);
    req.body.userId = sub;
    return next();
  } catch (error) {
    return res.status(403).json({ error: "Forbidden" });
  }
};
