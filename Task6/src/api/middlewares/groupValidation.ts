import { Request, Response } from "express";
import groupSchema from "./utils/validators/groupValidator";

export default ({ body }: Request, res: Response, next: Function) => {
  const { error } = groupSchema.validate(body);
  return error ? next(new Error(error.details[0].message)) : next();
};
