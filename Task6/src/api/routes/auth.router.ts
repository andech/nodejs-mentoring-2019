import express, { Router } from "express";
import { Container } from "typedi";
import AuthController from "../../controllers/Auth.controller";
import auth from "../middlewares/auth";

export default (router: express.Router) => {
  const controller: AuthController = Container.get("AuthController");
  const authRouter = Router()
    .post("/signin", controller.signIn.bind(controller))
    .post("/signout", auth, controller.signOut.bind(controller))
    .post("/refresh", controller.refresh.bind(controller));
  return router.use("/auth", authRouter);
};
