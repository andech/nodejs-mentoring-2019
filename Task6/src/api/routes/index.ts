import { Router } from "express";
import auth from "./auth.router";
import users from "./users.router";
import groups from "./groups.router";

export default () => {
  const router = Router();
  auth(router);
  users(router);
  groups(router);
  return router;
};
