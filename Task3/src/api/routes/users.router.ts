import { Router } from "express";
import { Container } from "typedi";
import UsersController from "../../controllers/Users.controller";
import userValidation from "../middlewares/userValidation";

export default Router()
  .get("/", (req, res, next) =>
    Container.get(UsersController).getAllUsers(req, res, next)
  )
  .get("/autosuggest", (req, res, next) =>
    Container.get(UsersController).getAutoSuggestUsers(req, res, next)
  )
  .get("/:id", (req, res, next) =>
    Container.get(UsersController).getUserById(req, res, next)
  )
  .post("/", userValidation, (req, res, next) =>
    Container.get(UsersController).createUser(req, res, next)
  )
  .put("/", userValidation, (req, res, next) =>
    Container.get(UsersController).updateUser(req, res, next)
  )
  .delete("/:id", (req, res, next) =>
    Container.get(UsersController).removeUser(req, res, next)
  );
