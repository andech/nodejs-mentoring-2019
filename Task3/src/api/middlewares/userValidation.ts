import { Request, Response } from "express";
import userSchema from "../../utils/userValidator";

export default ({ body }: Request, res: Response, next: Function) => {
  const { error } = userSchema.validate(body);
  return error ? next(new Error(error.details[0].message)) : next();
};
