import "reflect-metadata";
import express from "express";
import loaders from "./loaders";
import config from "./config";

async function startServer() {
  const app = express();
  const { port } = config.server;

  try {
    await loaders({ app });
  } catch (error) {
    console.log(error.message);
  }

  app.listen(port, () => console.log(`Server started on port ${port}`));
}

startServer();
