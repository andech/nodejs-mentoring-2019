import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import UsersService from "../services/Users.service";

@Service("UsersController")
export default class UsersController {
  @Inject("UsersService")
  usersService: UsersService;

  async getAllUsers(req: Request, res: Response, next: Function) {
    try {
      const users = await this.usersService.getAllUsers();
      return res.json(users);
    } catch (error) {
      return next(error);
    }
  }

  async getUserById(
    { params: { id } }: Request,
    res: Response,
    next: Function
  ) {
    try {
      const user = await this.usersService.getUser(id);
      return res.json(user);
    } catch (error) {
      return next(error);
    }
  }

  async createUser({ body }: Request, res: Response, next: Function) {
    try {
      const createdUser = await this.usersService.createUser(body);
      return res.json(createdUser);
    } catch (error) {
      return next(error);
    }
  }

  async updateUser({ body }: Request, res: Response, next: Function) {
    try {
      const updatedUser = await this.usersService.updateUser(body);
      return res.json(updatedUser);
    } catch (error) {
      return next(error);
    }
  }

  async removeUser({ params: { id } }: Request, res: Response, next: Function) {
    try {
      const changedUsers = await this.usersService.removeUser(id);
      return res.json(changedUsers);
    } catch (error) {
      return next(error);
    }
  }

  async getAutoSuggestUsers(
    { query: { loginSubstring, limit } }: Request,
    res: Response,
    next: Function
  ) {
    try {
      const suggestedUsers = await this.usersService.getAutoSuggestUsers(
        loginSubstring,
        +limit || 5
      );
      return res.json(suggestedUsers);
    } catch (error) {
      return next(error);
    }
  }
}
