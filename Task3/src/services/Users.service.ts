import { Inject, Service } from "typedi";
import { Op } from "sequelize";
import UserModel from "../database/models/User";
import { IUser } from "../interfaces/IUser";

@Service("UsersService")
export default class UsersService {
  @Inject("User")
  userModel: UserModel;

  async getAllUsers() {
    const foundUsers = await this.userModel.findAll();
    return foundUsers.map((user: UserModel) => user.get({ plain: true }));
  }

  async getUser(id: string) {
    const foundUser = await this.userModel.findOne({
      attributes: ["id", "login", "password", "age"],
      where: { id }
    });
    return foundUser || {};
  }

  async createUser(user: { login: string; password: string; age: number }) {
    const foundUser = await this.findUserByLogin(user.login);
    if (foundUser) {
      throw new Error("User with this login already exists");
    }
    const createdUser = await this.userModel.create(user, {
      fields: ["login", "password", "age"]
    });
    return createdUser.get({ plain: true });
  }

  async updateUser(user: IUser) {
    const foundUser = await this.findUserByLogin(user.login);
    if (foundUser && (foundUser.get({ plain: true }) as IUser).id !== user.id) {
      throw new Error("User with this login already exists");
    }
    const [, updatedUsers] = await this.userModel.update(user, {
      fields: ["login", "password", "age"],
      where: { id: user.id },
      returning: true
    });
    if (!updatedUsers.length) {
      throw new Error("There is no user with this id");
    }
    return updatedUsers[0].get({ plain: true });
  }

  async removeUser(id: string) {
    const userToDelete = await this.userModel.findOne({
      where: { id }
    });
    if (!userToDelete) {
      throw new Error("There is no user with this id");
    }
    await this.userModel.destroy({
      where: { id }
    });
    return this.getAllUsers();
  }

  async getAutoSuggestUsers(loginSubstring: string, limit: number) {
    const foundUsers = await this.userModel.findAll({
      where: {
        login: {
          [Op.like]: `%${loginSubstring}%`
        }
      },
      order: ["login"],
      limit
    });
    return foundUsers.map((user: UserModel) => user.get({ plain: true }));
  }

  async findUserByLogin(login: string) {
    return this.userModel.findOne({
      where: { login }
    });
  }
}
