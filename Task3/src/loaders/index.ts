import express from "express";
import dbLoader from "./dbLoader";
import appLoader from "./appLoader";
import "../services/Users.service";
import "../controllers/Users.controller";

export default async ({ app }: { app: express.Application }) => {
  await dbLoader();
  return appLoader({ app });
};
