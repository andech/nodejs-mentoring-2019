import { Sequelize } from "sequelize";
import { Container } from "typedi";
import config from "../config";
import models from "../database/models";

export default async () => {
  const sequelize = new Sequelize(
    config.db.database,
    config.db.username,
    config.db.password,
    { host: config.db.host, dialect: config.db.dialect }
  );
  try {
    await sequelize.authenticate();
  } catch (error) {
    console.log(error.message);
  }

  Object.values(models).forEach(model => {
    model.init(sequelize);
    Container.set(model.name, model);
  });

  Container.set("db", sequelize);
};
